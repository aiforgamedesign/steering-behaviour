﻿using System.Collections;
using UnityEngine;

public class WanderRandomSteering : MonoBehaviour
{

    [SerializeField]
    private float speed = 0.1f;
    [SerializeField]
    private float mass = 10;
    [SerializeField]
    private float timeUntilTargetSwitch = 1;
    [SerializeField]
    private float slowingRadius = 2;
    [SerializeField]
    private float radiusRandomPoint = 10;


    private Vector3 velocity;
    private Vector3 currentTarget;



    private void Start()
    {
        StartCoroutine(SwitchRandomTarget());
    }

    private void Update()
    {
        var velocity = Seek(currentTarget);
        transform.position += velocity;
        transform.forward = Vector3.Lerp(transform.forward, velocity.normalized, 0.1f);
    }


    IEnumerator SwitchRandomTarget()
    {
        while (true)
        {
            currentTarget = Random.onUnitSphere * Random.Range(1, radiusRandomPoint);
            currentTarget = new Vector3(currentTarget.x, currentTarget.y, 0);
            yield return new WaitForSeconds(timeUntilTargetSwitch);
        }
    }


    Vector3 Seek(Vector3 target)
    {
        var desiredVelocity = (target - transform.position).normalized * speed;
        


        var distance = Vector3.Distance(target, transform.position);
        if (distance < slowingRadius)
        {
            desiredVelocity = desiredVelocity * (distance / slowingRadius);
        }


        var steering = desiredVelocity - velocity;
        steering = steering / mass;

        return velocity + steering;
    }


    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(currentTarget, 1);
        Gizmos.DrawWireSphere(Vector3.zero, radiusRandomPoint);
    }


}
