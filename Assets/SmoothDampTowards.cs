﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothDampTowards : MonoBehaviour, IMoveable
{
    public Vector3 Velocity => velocity;

    private Vector3 velocity;


    public Vector3 Move(Vector3 target)
    {
        return Vector3.SmoothDamp(transform.position, target, ref velocity, 0.25f);
    }
}
