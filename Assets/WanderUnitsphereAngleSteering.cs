﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WanderUnitsphereAngleSteering : MonoBehaviour
{

    [SerializeField]
    private float speed = 0.1f;
    [SerializeField]
    private float mass = 10;
    [SerializeField]
    private float circleDistance = 1;
    [SerializeField]
    [Range(0, 90)]
    private float angleDisplacement = 1f;

    
    private float wanderAngle;
    private Vector3 velocity;


    private void Start()
    {
        velocity = Random.onUnitSphere * speed;
        velocity = new Vector3(velocity.x, velocity.y, 0);
    }

    private void Update()
    {
        velocity = Wander();
        transform.position += velocity;
        transform.forward = Vector3.Lerp(transform.forward, velocity.normalized, 0.01f);
    }


    Vector3 Wander()
    {
        var circleCenter = velocity.normalized;
        var rand = Random.value;
        wanderAngle += (rand * angleDisplacement - angleDisplacement * .5f) * 2;
        //Debug.Log(rand + " * " + angleDisplacement + " - " + angleDisplacement + " * 0.5  = " + wanderAngle);
        var scaledVelocity = (velocity.normalized * speed);
        var displacedVelocity = Quaternion.Euler(0, 0, wanderAngle) * scaledVelocity;
        var wanderForce = displacedVelocity + circleCenter * circleDistance;
        //Debug.Log(displacedVelocity + " + " + circleCenter + " * " + circleSize + " = " + wanderForce);
        return wanderForce;
    }

    private void OnDrawGizmos()
    {
        
    }


}
