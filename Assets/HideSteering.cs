﻿using UnityEngine;

public class HideSteering : MonoBehaviour
{
    [SerializeField()]
    private float maxVelocity = 0.5f;
    [SerializeField()]
    private float mass = 10;
    [SerializeField()]
    private float slowingRadius = 1.0f;
    [SerializeField()]
    private Vector3 currentVelocity;
    [SerializeField]
    private float avoidStrength = 0.01f;
    [SerializeField]
    private float distanceObstacleDetection = 0.1f;
    [SerializeField]
    private float offset = 0.05f;
    [SerializeField()]
    private WanderAvoidRaycastSteering[] enemies;

    private Vector3 currentTarget;
    private new MeshRenderer renderer;



    private void Start()
    {
        enemies = FindObjectsOfType<WanderAvoidRaycastSteering>();
        Debug.Log(enemies.Length);
        renderer = GetComponent<MeshRenderer>();
    }


    private void Update()
    {
        var enemy = CanSeeEnemy();
        if (enemy) {

            renderer.sharedMaterial.color = Color.blue;
            var hidingSpot = GetHidingTarget(enemy);
            currentTarget = hidingSpot;
        }
        else
        {
            renderer.sharedMaterial.color = Color.white;
        }
        currentVelocity = Seek(currentTarget);
        currentVelocity += Avoid(currentVelocity);
        transform.position += currentVelocity;
    }


    private Vector3 GetHidingTarget(WanderAvoidRaycastSteering enemy)
    {


        var directionToPlayer = transform.position - enemy.transform.position;
        RaycastHit hitInfo = new RaycastHit();
        bool isHit = Physics.Raycast(transform.position, directionToPlayer, out hitInfo, 100);
        if (isHit)
        {
            if(hitInfo.collider is SphereCollider)
            {
                var obstacle = hitInfo.collider as SphereCollider;
           
                float distAway = obstacle.radius + 0.06f;

                Vector3 dir = obstacle.transform.position - transform.position;
                dir.Normalize();

                return obstacle.transform.position + dir * distAway;
            }
        }



        //var directionToPlayer = transform.position - enemy.transform.position;
        //var directionToEnemy = enemy.transform.position - transform.position;
        //RaycastHit hitInfo = new RaycastHit();
        //bool isHit = Physics.Raycast(transform.position, directionToPlayer, out hitInfo, 100);
        //if (isHit)
        //{
        //    float stepSize = 0.05f;
        //    bool isHitBackwards = false;
        //    int stepCount = 0;
        //    while (!isHitBackwards)
        //    {
        //        if(stepCount > 100)
        //        {
        //            //limit
        //            break;
        //        }
        //        RaycastHit hitInfoBackwards = new RaycastHit();
        //        var stepPosition = hitInfo.point + directionToPlayer.normalized * stepSize * stepCount;
        //        var stepPositionEnd = hitInfo.point + directionToPlayer.normalized * stepSize * ((float)stepCount + 0.8f);
        //        isHitBackwards = Physics.Raycast(stepPosition, directionToEnemy, out hitInfoBackwards, stepSize);
        //        Debug.DrawLine(stepPosition, stepPositionEnd, Color.green, 1);
        //        if (isHitBackwards)
        //        {
        //            return hitInfoBackwards.point;
        //        }
        //        stepCount++;
        //    }
        //}
        return Vector3.zero;
    }

    private WanderAvoidRaycastSteering CanSeeEnemy()
    {
        foreach (var enemy in enemies)
        {
            var direction = enemy.transform.position - transform.position;
            RaycastHit hitInfo;
            bool isHit = Physics.Raycast(transform.position, direction, out hitInfo, 100);
            if (isHit)
            {
                Debug.Log(hitInfo.collider.gameObject);
                if (hitInfo.collider.gameObject == enemy.gameObject)
                {
                    Debug.DrawLine(transform.position, hitInfo.point, Color.cyan, 0.5f);
                    //Can see enemy
                    return enemy;
                }
            }
        }
        return null;

    }


    private Vector3 Seek(Vector3 target)
    {
        currentTarget = target;
        var desiredVelocity = (target - transform.position).normalized * maxVelocity;
        var distance = Vector3.Distance(target, transform.position);
        if (distance < slowingRadius)
        {
            desiredVelocity = desiredVelocity * (distance / slowingRadius);
        }
        var steering = desiredVelocity - currentVelocity;
        steering = steering / mass;

        var newVelocity = currentVelocity + steering;

        return newVelocity;
    }


    Vector3 Avoid(Vector3 velocity)
    {
        RaycastHit hitInfo = new RaycastHit();
        var isCollision = Physics.Raycast(transform.position - velocity.normalized * offset, velocity.normalized, out hitInfo, distanceObstacleDetection);
        if (isCollision)
        {
            if (hitInfo.collider)
            {
                var avoidForce = hitInfo.point - hitInfo.collider.transform.position;
                avoidForce.Scale(new Vector3(1, 1, 0));
                return avoidForce.normalized * avoidStrength;
            }
        }

        return Vector3.zero;

    }


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(currentTarget, 0.25f);
    }
}
