﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvadeSteering : MonoBehaviour, IMoveable
{
    public Vector3 Velocity => velocity;

    [SerializeField()]
    private float t = 3;
    [SerializeField()]
    private float mass = 15;
    [SerializeField()]
    private float speed = 1;
    [SerializeField()]
    private GameObject targetObj;

    private IMoveable mover;
    private Vector3 velocity;


    private void Start()
    {
        mover = targetObj.GetComponent<IMoveable>();
    }

    private void Update()
    {
        transform.position = transform.position + Move(targetObj.transform.position);
    }

    public Vector3 Move(Vector3 targetPos)
    {
        var dynamicT = Vector3.Distance(targetPos, transform.position) * t;
        var futurePosition = targetPos + (mover.Velocity * dynamicT);
        return Flee(futurePosition);
    }


    public Vector3 Flee(Vector3 target)
    {
        var targetDirection = (transform.position - target).normalized * speed;
        Debug.DrawLine(transform.position, targetDirection + transform.position, Color.yellow);
        var steering = targetDirection - velocity;
        steering = steering / mass;
        Debug.DrawLine(transform.position, steering + transform.position, Color.red);

        var desiredVelocity = velocity + steering;
        Debug.DrawLine(transform.position, desiredVelocity + transform.position, Color.green);

        Debug.DrawLine(transform.position, velocity + transform.position, Color.blue);
        velocity = desiredVelocity;
        return velocity;
    }


    private void OnDrawGizmos()
    {
        var dynamicT = Vector3.Distance(targetObj.transform.position, transform.position) * t;
        var futurePosition = targetObj.transform.position + (mover.Velocity * dynamicT);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(futurePosition, 0.5f);
    }
}
