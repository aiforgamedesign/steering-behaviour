﻿using UnityEngine;

public class LerpTowards : MonoBehaviour, IMoveable
{
    public Vector3 Velocity => Vector3.zero;

    public Vector3 Move(Vector3 target)
    {
        return Vector3.Lerp(transform.position, target, 4f * Time.deltaTime);
    }
}
