﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WanderUnitsphereSteering : MonoBehaviour
{

    [SerializeField]
    private float speed = 0.1f;
    [SerializeField]
    private float mass = 10;
    [SerializeField]
    private float displacementStrength = 0.0001f;


    private Vector3 velocity;

    private void Update()
    {
        var velocity = Wander();
        transform.position += velocity;
        transform.forward = Vector3.Lerp(transform.forward, velocity.normalized, 0.1f);
    }


    Vector3 Wander()
    {
        var displacement = Random.onUnitSphere * displacementStrength;
        displacement = new Vector3(displacement.x, displacement.y, 0);
        velocity = velocity / mass;
        velocity += displacement;
        velocity = velocity.normalized * speed;

        return velocity;
    }


}
