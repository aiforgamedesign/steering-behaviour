﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeekSteering : MonoBehaviour, IMoveable
{
    public Vector3 Velocity => velocity;


    [SerializeField()]
    private float maxVelocity = 0.5f;
    [SerializeField()]
    private float mass = 10;
    [SerializeField()]
    private Vector3 velocity;


    public Vector3 Move(Vector3 target)
    {
        var targetDirection = (target - transform.position).normalized * maxVelocity;
        Debug.DrawLine(transform.position, targetDirection + transform.position, Color.yellow);

        var steering = targetDirection - velocity;
        steering = steering / mass;
        Debug.DrawLine(transform.position, steering + transform.position, Color.red);

        var desiredVelocity = velocity + steering;
        Debug.DrawLine(transform.position, desiredVelocity + transform.position, Color.green);

        Debug.DrawLine(transform.position, velocity + transform.position, Color.blue);
        velocity = desiredVelocity;

        var newPosition = transform.position + desiredVelocity;

        return newPosition;
    }
}
