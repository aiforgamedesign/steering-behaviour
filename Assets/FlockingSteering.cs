﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Boid))]
public class FlockingSteering : MonoBehaviour
{

    [SerializeField]
    private float speed = 0.1f;
    [SerializeField]
    private float mass = 10;
    [SerializeField]
    private float timeUntilTargetSwitch = 1;
    [SerializeField]
    private float slowingRadius = 2;
    [SerializeField]
    private float radiusRandomPoint = 15;
    [SerializeField]
    private float radiusGroup = 5;
    [SerializeField]
    private float seekStrength = 1;
    [SerializeField]
    private float cohesionStrength = 1;
    [SerializeField]
    private float alignmentStrength = 1;
    [SerializeField]
    private float separationStrength = 1;
    [SerializeField()]
    private Boid[] allBoids;

    private Boid thisBoid;

    private Vector3 currentTarget;


    // Start is called before the first frame update
    void Start()
    {
        allBoids = FindObjectsOfType<Boid>();
        thisBoid = GetComponent<Boid>();
        StartCoroutine(SwitchRandomTarget());
    }

    // Update is called once per frame
    void Update()
    {
        var velocity = Seek(currentTarget) * seekStrength;
        Debug.DrawLine(transform.position, transform.position + velocity, Color.green);
        velocity += Flocking();
        Debug.DrawLine(transform.position, transform.position + velocity, Color.cyan);
        velocity = velocity.normalized * speed;
        Debug.DrawLine(transform.position, transform.position + velocity, Color.white);
        this.thisBoid.Velocity = velocity;
        transform.position += thisBoid.Velocity * speed;
    }

    private Vector3 Flocking()
    {
        Vector3 vAlignment = new Vector3();
        Vector3 vSeparation = new Vector3();
        Vector3 vCohesion = new Vector3();
        var neighborCount = 0;
        foreach (var boid in allBoids)
        {
            if (thisBoid != boid)
            {
                if (Vector3.Distance(thisBoid.transform.position, boid.transform.position) < radiusGroup)
                {
                    Debug.DrawLine(transform.position, boid.transform.position, new Color(1, 0, 0, 0.1f));
                    var xAlignment = vAlignment.x + boid.Velocity.x;
                    var yAlignment = vAlignment.y + boid.Velocity.y;
                    vAlignment = new Vector3(xAlignment, yAlignment, 0);

                    var xSeparation = vSeparation.x + (boid.transform.position.x - transform.position.x);
                    var ySeparation = vSeparation.y + (boid.transform.position.y - transform.position.y);
                    vSeparation = new Vector3(xSeparation, ySeparation, 0);


                    var xCohesion = vCohesion.x + boid.transform.position.x;
                    var yCohesion = vCohesion.y + boid.transform.position.y;
                    vCohesion = new Vector3(xCohesion, yCohesion, 0);



                    neighborCount++;
                }
            }
        }

        if (neighborCount != 0)
        {
            vAlignment.x /= neighborCount;
            vAlignment.y /= neighborCount;
            vAlignment.Normalize();

            vSeparation.x /= neighborCount;
            vSeparation.y /= neighborCount;
            vSeparation.x *= -1;
            vSeparation.y *= -1;
            vSeparation.Normalize();


            vCohesion.x /= neighborCount;
            vCohesion.y /= neighborCount;
            //Direction towards center of mass
            vCohesion = new Vector3(vCohesion.x - transform.position.x, vCohesion.y - transform.position.y, 0);
            vCohesion.Normalize();
        }


        vAlignment  *= alignmentStrength;
        vCohesion   *= alignmentStrength;
        vSeparation *= separationStrength;

        var thisX = vAlignment.x + vCohesion.x + vSeparation.x;
        var thisY = vAlignment.y + vCohesion.y + vSeparation.y;

        var flockingForce = new Vector3(thisX, thisY, 0);
        flockingForce /= mass;
        Debug.DrawLine(transform.position, transform.position + flockingForce, Color.red);

        return flockingForce;
    }

    IEnumerator SwitchRandomTarget()
    {
        while (true)
        {
            currentTarget = Random.onUnitSphere * Random.Range(1, radiusRandomPoint);
            currentTarget = new Vector3(currentTarget.x, currentTarget.y, 0);
            yield return new WaitForSeconds(timeUntilTargetSwitch);
        }
    }


    Vector3 Seek(Vector3 target)
    {
        var desiredVelocity = (target - transform.position).normalized * speed;



        var distance = Vector3.Distance(target, transform.position);
        if (distance < slowingRadius)
        {
            desiredVelocity = desiredVelocity * (distance / slowingRadius);
        }


        var steering = desiredVelocity - thisBoid.Velocity;
        steering = steering / mass;

        return steering;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(currentTarget, slowingRadius);
        Gizmos.DrawWireSphere(Vector3.zero, radiusRandomPoint);
        Gizmos.color = new Color(0, 1, 1, 0.1f);
        Gizmos.DrawWireSphere(transform.position, radiusGroup);
    }
}
