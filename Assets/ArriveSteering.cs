﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArriveSteering : MonoBehaviour, IMoveable
{
    public Vector3 Velocity => velocity;



    [SerializeField()]
    private float maxVelocity = 0.5f;
    [SerializeField()]
    private float mass = 10;
    [SerializeField()]
    private float slowingRadius = 1.0f;
    [SerializeField()]
    private Vector3 velocity;

    private Vector3 currentTarget;


    public Vector3 Move(Vector3 target)
    { 
        currentTarget = target;
        var desiredVelocity = (target - transform.position).normalized * maxVelocity;
        var distance = Vector3.Distance(target, transform.position);
        if(distance < slowingRadius)
        {
            desiredVelocity = desiredVelocity * (distance / slowingRadius);
        }
        var steering = desiredVelocity - velocity;
        steering = steering / mass;
        
        velocity = velocity + steering;

        var newPosition = transform.position + velocity;

        return newPosition;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(currentTarget, slowingRadius);
    }
}
