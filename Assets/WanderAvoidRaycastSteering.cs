﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WanderAvoidRaycastSteering : MonoBehaviour
{

    [SerializeField]
    private float speed = 0.1f;
    [SerializeField]
    private float mass = 10;
    [SerializeField]
    private float timeUntilTargetSwitch = 1;
    [SerializeField]
    private float slowingRadius = 2;
    [SerializeField]
    private float radiusRandomPoint = 10;
    [SerializeField]
    private float distanceObstacleDetection = 3;
    [SerializeField]
    private float distanceObstacleDetectionClose = 0.1f;
    [SerializeField]
    private float offset = 1;
    [SerializeField]
    private float avoidStrength = 0.01f;


    private Vector3 velocity;
    private Vector3 currentTarget;
    private Vector3 lastCollisionPoint;
    private Vector3 closeCollider;



    private void Start()
    {
        StartCoroutine(SwitchRandomTarget());
    }

    private void Update()
    {
        velocity = Seek(currentTarget);
        velocity += Avoid(velocity);
        transform.position += velocity;
        transform.forward = Vector3.Lerp(transform.forward, velocity.normalized, 0.1f);
    }


    IEnumerator SwitchRandomTarget()
    {
        while (true)
        {
            currentTarget = Random.onUnitSphere * Random.Range(1, radiusRandomPoint);
            currentTarget = new Vector3(currentTarget.x, currentTarget.y, 0);
            yield return new WaitForSeconds(timeUntilTargetSwitch);
        }
    }


    Vector3 Avoid(Vector3 velocity)
    {

        //Collider closestCollider = null;
        //float closestDistance = 10;
        //Collider[] hitColliders = Physics.OverlapSphere(transform.position, distanceObstacleDetectionClose);
        //foreach (var collider in hitColliders)
        //{
        //    var distance = Vector3.Distance(collider.transform.position, collider.transform.position);
        //    if (closestDistance > distance)
        //    {
        //        closestCollider = collider;
        //    }
        //}

        //if (closestCollider)
        //{
        //    closeCollider = closestCollider.transform.position;
        //    var avoidForce = transform.position - closestCollider.transform.position;
        //    return avoidForce.normalized * avoidStrength;
        //}



        RaycastHit hitInfo = new RaycastHit();
        var isCollision = Physics.Raycast(transform.position - velocity.normalized * offset, velocity.normalized, out hitInfo, distanceObstacleDetection);
        if (isCollision)
        {
            if (hitInfo.collider)
            {
                lastCollisionPoint = hitInfo.point;
                var avoidForce = hitInfo.point - hitInfo.collider.transform.position;
                avoidForce.Scale(new Vector3(1, 1, 0));
                return avoidForce.normalized * avoidStrength;
            }
        }

        return Vector3.zero;
        
    }


    Vector3 Seek(Vector3 target)
    {
        var desiredVelocity = (target - transform.position).normalized * speed;



        var distance = Vector3.Distance(target, transform.position);
        if (distance < slowingRadius)
        {
            desiredVelocity = desiredVelocity * (distance / slowingRadius);
        }


        var steering = desiredVelocity - velocity;
        steering = steering / mass;

        return velocity + steering;
    }


    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(currentTarget, slowingRadius);
        Gizmos.DrawWireSphere(Vector3.zero, radiusRandomPoint);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(lastCollisionPoint, 0.5f);
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(Vector3.Scale(closeCollider, new Vector3(1, 1, -10)), distanceObstacleDetectionClose );
        var offsetScaled = velocity.normalized * offset;
        Gizmos.DrawLine(transform.position - offsetScaled, transform.position + velocity.normalized * distanceObstacleDetection - offsetScaled);
        
    }


}
