﻿using UnityEngine;

public class MoveTowards : MonoBehaviour, IMoveable
{
    public Vector3 Velocity => Vector3.zero;

    public Vector3 Move(Vector3 target)
    {
        return Vector3.MoveTowards(transform.position, target, 4.5f * Time.deltaTime);
    }
}
