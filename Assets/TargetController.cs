﻿using UnityEngine;

public class TargetController : MonoBehaviour
{

    [SerializeField()]
    private GameObject[] targets;

    [SerializeField()]
    private float margin = 0.001f;
    
    private IMoveable mover; 
    private int currentTargetIndex;


    
    void Start()
    {
        mover = GetComponent<IMoveable>();
        currentTargetIndex = 0;
    }

    
    void Update()
    {
        var currentTarget = targets[currentTargetIndex].transform.position;
        var currentPos = mover.Move(currentTarget);

        transform.position = currentPos;

        if(Vector3.Distance(currentPos, currentTarget) <= margin)
        {
            currentTargetIndex = (int)Mathf.Repeat(currentTargetIndex + 1, targets.Length);
        }

    }

    private void OnDrawGizmos()
    {
        foreach(var target in targets)
        {
            Gizmos.DrawWireSphere(target.transform.position, margin);
        }
    }
}
