﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WanderAndAvoidSteering : MonoBehaviour
{

    [SerializeField]
    private float speed = 0.1f;
    [SerializeField]
    private float mass = 10;
    [SerializeField]
    private float timeUntilTargetSwitch = 1;
    [SerializeField]
    private float slowingRadius = 2;
    [SerializeField]
    private float radiusRandomPoint = 10;
    [SerializeField]
    private float distanceObstacleDetection = 3;
    [SerializeField]
    private float avoidStrength = 1;
    [SerializeField]
    private SphereCollider[] obstacles;


    private Vector3 velocity;
    private Vector3 currentTarget;



    private void Start()
    {

        obstacles = FindObjectsOfType<SphereCollider>();
        StartCoroutine(SwitchRandomTarget());
    }

    private void Update()
    {
        velocity = Seek(currentTarget);
        velocity += Avoid();
        transform.position += velocity;
        transform.forward = Vector3.Lerp(transform.forward, velocity.normalized, 0.1f);
    }


    IEnumerator SwitchRandomTarget()
    {
        while (true)
        {
            currentTarget = Random.onUnitSphere * Random.Range(1, radiusRandomPoint);
            currentTarget = new Vector3(currentTarget.x, currentTarget.y, 0);
            yield return new WaitForSeconds(timeUntilTargetSwitch);
        }
    }


    Vector3 Avoid()
    {

        

        foreach(SphereCollider obstacle in obstacles)
        {

        }


        return Vector3.zero;
        
    }


    Vector3 Seek(Vector3 target)
    {
        var desiredVelocity = (target - transform.position).normalized * speed;



        var distance = Vector3.Distance(target, transform.position);
        if (distance < slowingRadius)
        {
            desiredVelocity = desiredVelocity * (distance / slowingRadius);
        }


        var steering = desiredVelocity - velocity;
        steering = steering / mass;

        return velocity + steering;
    }


    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(currentTarget, 1);
        Gizmos.DrawWireSphere(Vector3.zero, radiusRandomPoint);
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(transform.position, transform.position + velocity);


        Gizmos.color = new Color(0, 0.666f, 0, 0.333f);
        foreach(var obstacle in obstacles)
        {
            Gizmos.DrawWireSphere(obstacle.center + obstacle.transform.position, obstacle.radius * obstacle.transform.localScale.x);
        }
        
    }


}
